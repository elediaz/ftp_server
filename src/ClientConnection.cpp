//****************************************************************************
//                         REDES Y SISTEMAS DISTRIBUIDOS
//                      
//                     2ª de grado de Ingeniería Informática
//                       
//              This class processes an FTP transactions.
// 
//****************************************************************************



#include <cstring>
#include <cstdarg>
#include <cstdio>
#include <cerrno>
#include <netdb.h>
#include <fstream>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <locale.h>
#include <langinfo.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <sys/stat.h>
#include <iostream>
#include <dirent.h>

#include "common.h"

#include "ClientConnection.h"

#ifdef DEBUG
#define trace(msg) std::cout << msg << std::endl;
#else
#define trace(msg)
#endif // DEBUG

const char * cmdlist_str[] = {
  "USER", "PASS", "PORT", "PASV", "STOR", "RETR", "SYST", "TYPE",
  "LIST", "PWD", "QUIT"
};

int stringToCommand(char * cmd) {
  int command = NotImplemented;
  bool found = false;
  int i = 0;

  while (!found || (sizeof(cmdlist_str)/sizeof(char *) <= i)) {
    found = strcmp(cmdlist_str[i], cmd)==0;
    i++;
  }
  if (found) {
    command = i-1;
  }
  return command;
}

int connect_TCP( uint32_t address,  uint16_t  port) {
  // Implement your code to define a socket here
  struct sockaddr_in sin;
  //struct hostent *hent;
  int s;

  memset(&sin, 0, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_port = htons(port);
  sin.sin_addr.s_addr = htonl(address);

  trace("IP To Connect:" << inet_ntoa(sin.sin_addr));

  s = socket(AF_INET, SOCK_STREAM, 0);
  if(s < 0)
    errexit("No se puede crear el socket: %s\n", strerror(errno));

  if(connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    errexit("No se puede conectar: %s\n",  strerror(errno));

  return s;
}

int define_socket_TCP(int port) {
  struct sockaddr_in sin;
  int s;

  s = socket(AF_INET,SOCK_STREAM, 0);
  trace("Defining port")

  if(s < 0) {
    errexit("No puedo crear el socket: %s\n", strerror(errno));
  }

  memset(&sin, 0, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons(port);

  if(bind(s, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
    errexit("No puedo hacer el bind con el puerto: %s\n", strerror(errno));
  }

  if (listen(s, 5) < 0)
    errexit("Fallo en el listen: %s\n", strerror(errno));
  return s;
}


ClientConnection::ClientConnection(int s) {
  struct sockaddr_in sin;

  _control_socket = s;
  int len = sizeof(sin);
  getsockname(_control_socket, (struct sockaddr *)&sin, (socklen_t *)&len);
  // Get ip
  _ip = ntohl(sin.sin_addr.s_addr);
  trace("Start clietn Connection IP: " << _ip);
  // Check the Linux man pages to know what fdopen does.
  _fd = fdopen(s, "a+");
  if (_fd == NULL){
    trace("Connection closed");

    fclose(_fd);
    close(_control_socket);
    _ok = false;
    return;
  }
  _ok = true;
  _data_socket = -1;
};


ClientConnection::~ClientConnection() {
 	fclose(_fd);
	close(_control_socket);
}



void ClientConnection::stop() {
    close(_data_socket);
    close(_control_socket);
    _stop = true;
}

// This method processes the requests.
// Here you should implement the actions related to the FTP commands.
// See the example for the USER command.
// If you think that you have to add other commands feel free to do so. You
// are allowed to add auxiliary methods if necessary.
void ClientConnection::WaitForRequests() {
  if (!_ok)
    return;

  fprintf(_fd, "220 Service ready\n");

  while(!_stop) {
    fscanf(_fd, "%s", _command);
    trace("Recieve Command: " << _command);
    switch (stringToCommand(_command)) {
    case USER: cmd_user(); break;
    case PASS: cmd_pass(); break;
    case PORT: cmd_port(); break;
    case PASV: cmd_pasv(); break;
    case STOR: cmd_stor(); break;
    case RETR: cmd_retr(); break;
    case SYST: cmd_syst(); break;
    case TYPE: cmd_type(); break;
    case LIST: cmd_list(); break;
    case PWD: cmd_pwd(); break;
    case QUIT: cmd_quit(); break;
    default:
	    fprintf(_fd, "502 Command not implemented.\n"); fflush(_fd);
	    printf("Comando : %s %s\n", _command, _arg);
	    printf("Error interno del servidor\n");
      break;
    }
  }
  fclose(_fd);
  return;
}

void ClientConnection::cmd_user() {
  // USER <username>
  // hacer un si esta usuario introduzca key sino dar error
  fscanf(_fd, "%s", _arg);
  fprintf(_fd, "331 User name ok, need password\n");
  fflush(_fd);
}

void ClientConnection::cmd_pass() {
  // PASS <password>
  // Se pide contraseña y estamos logueados
  fscanf(_fd, "%s", _arg);
  _usernameOK = true;
  fprintf(_fd, "230 User logged in, proceed\n");
}

void ClientConnection::cmd_port() {
  // EL comando se usa para establecer una conexcion con el
  // cliente desde el servidor. Por ello el cliente nos manda
  // su puerto junto con su ip codificados.
  // El formato de codficacion es:
  // PORT h1,h2,h3,h4,p1,p2
  // donde hX u PX son enteros de 8 bits
  int p1, p2, h1, h2, h3, h4;
  fscanf(_fd,"%d,%d,%d,%d,%d,%d",&h1,&h2,&h3,&h4,&p1,&p2);
  // trace("PORT Command IP: " << h1 "." << h2 << "." << h3 << "." << h4 << "Port: " << (p1 << 8 | p2));
  uint32_t host = h1 << 24 | h2 << 16 | h3 << 8 | h4;
  _data_socket = connect_TCP(host, (p1 << 8 | p2));
  if (_data_socket > 0)
    fprintf(_fd, "202 Command okey\n");
  else
    fprintf(_fd, "421 Service not available, closing control connection.\n");

  _mode = Active;
}

void ClientConnection::cmd_pasv() {
  // PASV
  // este comando solicita al servidor DTP a "escuchar" en un
  // puerto de datos(el cual no es por puerto de datos por defecto) y
  // espera por una coneccion mas que por iniciar la recepcion de
  // un comando de tranferencia. LA repuesta  a este comando incluye
  // el host y el puerto destino que este servidor esta escuchando

  close(_data_socket); // cerramos por si acaso
  struct sockaddr_in sin;

  // Use the ip got to connect with a new random port (explain 0)
  _data_socket = define_socket_TCP(0); // definesocket
  if (_data_socket > 0) {
    int len = sizeof(sin);
    getsockname(_data_socket, (struct sockaddr *)&sin, (socklen_t *)&len);
    // Get ip
    // I need the random port that it choose
    uint16_t auxP = ntohs(sin.sin_port);
    unsigned char * lPort = (unsigned char *)&auxP;
    // Get the ip from raw format
    unsigned char * lIP = (unsigned char *)&_ip;

    //trace("IP Destino: "<< int(lIP[3]) << "." << int(lIP[2]) << "." << int(lIP[1]) << "." << int(lIP[0]));
    //trace("Puerto Listen: "<< auxP << "  " << sin.sin_port);
    fprintf(_fd,"227 Entering passive mode (%d,%d,%d,%d,%d,%d)\n",
            lIP[3],lIP[2],lIP[1],lIP[0],
            lPort[1], lPort[0]);

    _mode = Passive;
  }
  else {
    fprintf(_fd,"500 Error interno");
  }
}

void ClientConnection::cmd_stor() {
  // STOR <pathname>
  // acepta los datos transferidos por medio de la coneccion de datos
  // y guardarlos en el lugar del sevidor, se remplaza el contenido
  // existente y si no existe se crea

  fscanf(_fd, "%s", _arg);
  trace("FILE: " << _arg);

  std::ofstream file(_arg);
  if (!file.is_open()){
    fprintf(_fd, "450 Requested file action not taken. File unavaible.\n");
    close(_data_socket);
  }

  else{
    fprintf(_fd, "150 File status okay; about to open data connection.\n");
    fflush(_fd);

    int addrlen = 0;
    struct sockaddr_in client_address;
    addrlen = sizeof(client_address);
    char buffer[MAX_BUFF];
    int n;

    switch(_mode) {
    case Active:
      do{
        n = recv(_data_socket, buffer, MAX_BUFF, 0);
        file.write(buffer, n);
        trace("Tamaño de recv: " << n);
        trace("n == MAX_BUFF: " << n << "   " << MAX_BUFF);
      } while (n == MAX_BUFF);
      break;
    case Passive:
      int connection = accept(_data_socket,
                              (struct sockaddr*) &client_address,
                              (socklen_t *)&addrlen);
      do{
        n = recv(connection, buffer, MAX_BUFF, 0);
        file.write(buffer, n);
      } while (n == MAX_BUFF);
      close(connection);
      break;
    }

    fprintf(_fd,"226 Closing data connection. Requested file action successful.\n");
    file.close();
    close(_data_socket);
  }
}

void ClientConnection::cmd_retr() {
  // RETR <pathname>
  // provoca que el servidor transfiera una copia del archivo
  // especifficado al servidor o usuario en el otro lado.
  // no se debe modificar los archivos del servidor
  char pathname[50];
  fscanf(_fd, "%s", pathname);
  trace("FILE: " << std::string(pathname));
  std::ifstream file(pathname);
  if (file.is_open()) {
    fprintf(_fd, "150 File status okay; about to open data connection\n");
    file.seekg (0, file.end);
    int n = file.tellg();
    file.seekg (0, file.beg);
    char buffer[n];

    switch (_mode) {
    case Active:
      file.read(buffer, n);
      send(_data_socket, buffer, n, 0);
      break;
    case Passive:
      int addrlen = 0;
      struct sockaddr_in client_address;
      addrlen = sizeof(client_address);
      int connection = accept(_data_socket,
                              (struct sockaddr*) &client_address,
                              (socklen_t *)&addrlen);
      file.read(buffer, n);
      send(connection, buffer, n, 0);
      close(connection);
      break;
    }
  }
  else {
    fprintf(_fd, "451 Requested action aborted. Local error in processing.\n");
  }
  file.close();
  close(_data_socket);
  fprintf(_fd, "226 Closing data connection.\n");
}

void ClientConnection::cmd_syst() {
  // SYST
  // Specific OS used
  // See for more information http://tools.ietf.org/pdf/rfc943
  fprintf(_fd, "215 UNIX Type: L8.\n");
}

void ClientConnection::cmd_type() {
  // TYPE <type-code>
  // Todo esplicar este comando
  fprintf(_fd, "200 Command okay\n");
}

void ClientConnection::cmd_list() {
  // LIST [<pathname>]
  // el comando puede especificar un directorio
  // o un conjunto de archivos, el servidor debe
  // listar(ls) los archivos del directorio
  // scanf
  DIR *dir;
  struct dirent *ent;
  fprintf(_fd, "125 List Started OK\n");
  if ((dir = opendir (".")) != NULL) {
    /* print all the files and directories within directory */
    switch (_mode) {
    case Active:
      while ((ent = readdir (dir)) != NULL) {
        std::string aux = std::string(ent->d_name) + "\n";
        send(_data_socket, aux.c_str(), aux.length(), 0);
      }
      fprintf(_fd, "250 List Completed successfully\n");
      closedir(dir);
      break;
    case Passive:
      int addrlen = 0;
      struct sockaddr_in client_address;
      addrlen = sizeof(client_address);
      int connection = accept(_data_socket,(struct sockaddr*) &client_address,(socklen_t *)&addrlen);
      while ((ent = readdir (dir)) != NULL) {
        std::string aux = std::string(ent->d_name) + "\n";
        send(connection, aux.c_str(), aux.length(), 0);
      }
      fprintf(_fd, "250 List Completed successfully\n");
      fflush(_fd);
      closedir(dir);
      close(connection);
      break;
    }
  } else {
    /* could not open directory */
    fprintf(_fd,"XXX Error buscar\n");
  }
  close(_data_socket);
}

void ClientConnection::cmd_pwd() {
  // PWD
  // imprimir el directorio hacia el cliente
  char * path;
  path = getcwd(NULL,0); // unistd.h
  fprintf(_fd, "257 %s.\n",path);
}

void ClientConnection::cmd_quit() {
  // QUIT
  // sale del programa si no hay una transferencia de archivos
  // corriendo
  fprintf(_fd, "221 Service closing control connection.\n");
  stop();
}

