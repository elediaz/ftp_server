#if !defined ClientConnection_H
#define ClientConnection_H

#include <pthread.h>

#include <cstdio>
#include <cstdint>

const int MAX_BUFF=1000;

enum Mode {
  Passive,
  Active
};

enum Command {
  USER, PASS, PORT, PASV, STOR, RETR, SYST, TYPE, LIST, PWD, QUIT,
  NotImplemented
};

int stringToCommand(char * cmd);

int connect_TCP(uint32_t, uint16_t);

int define_socket_TCP(int);

// | This class is the controller of commands receive from client
//   and its response.
class ClientConnection {
 public:
  ClientConnection(int s);
  ~ClientConnection();

  void WaitForRequests();
  void stop();

 private:
  void cmd_user();
  void cmd_pass();
  void cmd_port();
  void cmd_pasv();
  void cmd_stor();
  void cmd_retr();
  void cmd_syst();
  void cmd_type();
  void cmd_list();
  void cmd_pwd();
  void cmd_quit();

 private:
  bool _stop;
  // ^ rename parar
  bool _ok;
  // ^ This variable is flag that avois that the
  //   server listens if initialization errors occured.
  bool _usernameOK;
  // ^ Is logged?
  uint32_t _ip;
  // ^ IP of server
  Mode _mode;
  // ^ Current mode to use
  FILE *_fd;
  // ^ C file descriptor. We use it to buffer the
  //   control connection of the socket and allows to
  //   manage it as a C file usign fprintf, fscanf, etc.

  char _command[MAX_BUFF];
  // ^ Buffer for saving the command.
  char _arg[MAX_BUFF];
  // ^ Buffer for saving the arguments.

  int _data_socket;
  // ^ Data socket descriptor;
  int _control_socket;
  // ^ Control socket descriptor;

};

#endif
